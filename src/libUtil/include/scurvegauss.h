#ifndef SCURVEGAUSS_H
#define SCURVEGAUSS_H

void scurvegauss(double* par, const int n_points, const double* x, const double *y);

#endif